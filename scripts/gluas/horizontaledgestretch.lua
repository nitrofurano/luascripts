leftempty=0;rightempty=0
function INT(a)
  return(a-a%1)
  end
function COPY(tx1,ty1,tx2,ty2,tx3,ty3)
  for ty4=ty1,ty2,1 do
    for tx4=tx1,tx2,1 do
      r,g,b=get_rgb(tx4,ty4)
      set_rgb(tx3+tx4-tx1,ty3+ty4-ty1,r,g,b)
      end;end
  end
xver0=0
for x=0,width-1,1 do
  r0,g0,b0=get_rgb(x,height*.33)
  r1,g1,b1=get_rgb(x,height*.66)
  if ((r0+g0+b0)~=(r1+g1+b1)) and leftempty==0 then
    xver0=x;leftempty=1
    end
  progress((x/width)*.25)
  end
if leftempty==1 then
  for x=0,xver0-1,1 do
    COPY(xver0,0,xver0,height-1,x,0)
    progress(.25+((x/xver0)*.25))
    end;end
xver1=0
for x=width-1,0,-1 do
  r0,g0,b0=get_rgb(x,height*.33)
  r1,g1,b1=get_rgb(x,height*.66)
  if ((r0+g0+b0)~=(r1+g1+b1)) and rightempty==0 then
    xver1=x;rightempty=1
    end
  progress(.5+((width-x)/width)*.25)
  end
if rightempty==1 then
  for x=xver1+1,width-1,1 do
    COPY(xver1,0,xver1,height-1,x,0)
    progress(.75+((x-xver1)/(width-xver1))*.25)
    end;end

