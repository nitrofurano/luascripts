rc=1;gc=0;bc=0
for y=0,height-1,1 do
  for x=0,(width/2)-1,1 do
    r0,g0,b0=get_rgb(x,y)
    r1,g1,b1=get_rgb((width/2)+x,y)
    if rc==1 then
      rs=r0;r0=r1;r1=rs
      end
    if gc==1 then
      gs=g0;g0=g1;g1=gs
      end
    if bc==1 then
      bs=b0;b0=b1;b1=bs
      end
    set_rgb(x,y,r0,g0,b0)
    set_rgb((width/2)+x,y,r1,g1,b1)
    end
    progress(y/height)
  end

