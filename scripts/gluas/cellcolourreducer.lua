xcell=8;ycell=8 --bug: innacurate greenish result when ycell=1 or =2
for y1=0,height-1,ycell do
  for x1=0,width-1,xcell do
    wcnt=0
    for y2=0,ycell-1,1 do
      for x2=0,xcell-1,1 do
        x=x1+x2;y=y1+y2
        r,g,b=get_rgb(x,y)
        w=((b*11)+(r*30)+(g*59))/100
        wcnt=wcnt+w
        end;end
    wcnt=wcnt/(xcell*ycell)
    r0cnt=0;g0cnt=0;b0cnt=0;t0cnt=0;r1cnt=0;g1cnt=1;b1cnt=0;t1cnt=0
    for y2=0,ycell-1,1 do
      for x2=0,xcell-1,1 do
        x=x1+x2;y=y1+y2
        r,g,b=get_rgb(x,y)
        w=((b*11)+(r*30)+(g*59))/100
        if w<wcnt then
          r0cnt=r0cnt+r;g0cnt=g0cnt+g;b0cnt=b0cnt+b;t0cnt=t0cnt+1
        else
          r1cnt=r1cnt+r;g1cnt=g1cnt+g;b1cnt=b1cnt+b;t1cnt=t1cnt+1
          end
        end;end
    r0cnt=r0cnt/t0cnt;g0cnt=g0cnt/t0cnt;b0cnt=b0cnt/t0cnt
    r1cnt=r1cnt/t1cnt;g1cnt=g1cnt/t1cnt;b1cnt=b1cnt/t1cnt
    for y2=0,ycell-1,1 do
      for x2=0,xcell-1,1 do
        x=x1+x2;y=y1+y2
        r,g,b=get_rgb(x,y)
        w=((b*11)+(r*30)+(g*59))/100
        if w<wcnt then
          set_rgb(x,y,r0cnt,g0cnt,b0cnt)
        else
          set_rgb(x,y,r1cnt,g1cnt,b1cnt)
          end
        end;end
    end
  progress(y1/height)
  end

