for y=0,height-1,1 do
  for x=0,width-1,3 do
    r0,g0,b0=get_rgb(x+0,y)
    r1,g1,b1=get_rgb(x+1,y)
    r2,g2,b2=get_rgb(x+2,y)
    set_rgb(x+0,y,r0,0,0)
    set_rgb(x+1,y,0,g1,0)
    set_rgb(x+2,y,0,0,b2)
    end
  progress(y/height)
  end

