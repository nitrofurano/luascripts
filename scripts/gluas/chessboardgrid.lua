-- chessboardgrid
xsize=8;ysize=16
ik=0x550000;pa=0x005500
function INT(a)
  return(a-a%1);end
r1=(INT(pa/65536))%256;g1=(INT(pa/256))%256;b1=pa%256
for y=0,height-1 do
  for x=0,width-1 do
    set_rgb(x,y,r1/255,g1/255,b1/255);end;end
r1=(INT(ik/65536))%256;g1=(INT(ik/256))%256;b1=ik%256
for y=0,height-1 do
  for x=0,width-1 do
    q=(((INT(x/xsize))%2)+((INT(y/ysize))%2))%2
    if q==0 then
      set_rgb(x,y,r1/255,g1/255,b1/255);end;end;end

