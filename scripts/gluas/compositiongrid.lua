for q=0,height-1,1 do
  set_rgb(width*.25,q,1,0,0)
  set_rgb(width*.5,q,1,0,0)
  set_rgb(width*.75,q,1,0,0)
  set_rgb(width*.618,q,0,1,0)
  set_rgb(width*.382,q,0,1,0)
  end
for q=0,width-1,1 do
  set_rgb(q,height*.25,1,0,0)
  set_rgb(q,height*.5,1,0,0)
  set_rgb(q,height*.75,1,0,0)
  set_rgb(q,height*.618,0,1,0)
  set_rgb(q,height*.382,0,1,0)
  end
if width>height then
  for q=0,height-1,1 do
    set_rgb(q,q,0,0,1)
    set_rgb(q,height-q,0,0,1)
    set_rgb(width-q,q,0,0,1)
    set_rgb(width-q,height-q,0,0,1)
    end
else
  for q=0,width-1,1 do
    set_rgb(q,q,0,0,1)
    set_rgb(q,height-q,0,0,1)
    set_rgb(width-q,q,0,0,1)
    set_rgb(width-q,height-q,0,0,1)
    end
  end
flush()

