xc=4;yc=1;lp2=32;ap2=64;bp2=64 -- lp=32;ap=64;bp=64 -- lp=16;ap=64;bp=64
lp=255/lp2;ap=255/ap2;bp=255/bp2
function INT(a)
  return(a-a%1)
  end
function SET_YJK(tx0,ty0,tyc0,tjc0,tkc0)
  tr0=tyc0+tjc0;tg0=tyc0+tkc0
  tb0=(tyc0*1.25)-(tjc0*.5)-(tkc0*.25)
  set_rgb(tx0,ty0,tr0,tg0,tb0)
  end
function GET_YJK(tx0,ty0)
  tr0,tg0,tb0=get_rgb(tx0,ty0)
  tyc0=(tb0/2)+(tr0/4)+(tg0/8)
  tjc0=tr0-tyc0;tkc0=tg0-tyc0
  return tyc0,tjc0,tkc0
  end
for y1=0,height-1,yc do
  progress(y1/height)
  for x1=0,width-1,xc do
    as=0;bs=0;sd=0
    for y2=0,yc-1,1 do
      for x2=0,xc-1,1 do
        x=x1+x2;y=y1+y2
        lq,aq,bq=GET_YJK(x,y)
        lr=lq*255;ar=(127*aq)+128;br=(127*bq)+128
        as=as+ar;bs=bs+br;sd=sd+1
        end;end
    af=as/sd;bf=bs/sd
    az=(INT(af/ap))*ap
    bz=(INT(bf/bp))*bp
    for y2=0,yc-1,1 do
      for x2=0,xc-1,1 do
        x=x1+x2;y=y1+y2
        lq,aq,bq=GET_YJK(x,y)
        lr=lq*255;ar=(127*aq)+128;br=(127*bq)+128
        lz=(INT(lr/lp))*lp
        SET_YJK(x,y,(lz/255)*1.05,(az/127)-1,(bz/127)-1)
        end;end;end;end
flush()

