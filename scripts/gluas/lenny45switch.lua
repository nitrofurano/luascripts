cellsz=8;ticknets=2
function INT(a)
  return(a-a%1)
  end
function BAR(tx1,ty1,tx2,ty2,tcl)
  tr1=(INT(tcl/65536))%256
  tg1=(INT(tcl/256))%256
  tb1=tcl%256
  for tx3=tx1,tx2,1 do
    for ty3=ty1,ty2,1 do
      set_rgb(tx3,ty3,tr1/255,tg1/255,tb1/255)
    end;end
  end
function RND(tseed)
  tvl1=INT(tseed/256)
  tvl2=(((tseed%256)*256)+253)-(tseed*2)
  tseed=tseed%256+tvl1
  tvl2=tvl2-tseed
  while tvl2<0 do tvl2=tvl2+65537 end
  tvl2=tvl2%65536
  return tvl2
  end
seed=0
for x1=0,width-1,cellsz do
  for y1=0,height-1,cellsz do
    BAR(x1,y1,x1+cellsz-1,y1+cellsz-1,0xFFFFFF)
    seed=RND(seed)
    if (seed%256)>128 then
      for x2=0,cellsz-ticknets,1 do
        BAR(x1+x2,y1+x2,x1+x2+ticknets-1,y1+x2+ticknets-1,0x000000)
        end
    else
      for x2=0,cellsz-ticknets,1 do
        BAR(x1+x2,y1+cellsz-x2-2,x1+x2+ticknets-1,y1+cellsz-x2+ticknets-3,0x000000)
        end
      end
    end;end

