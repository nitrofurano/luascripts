--enlarge canvas size first
function INT(a)
  return(a-a%1)
  end
function BAR(tx1,ty1,tx2,ty2,tcl)
  tr1=(INT(tcl/65536))%256
  tg1=(INT(tcl/256))%256
  tb1=tcl%256
  for tx3=tx1,tx2,1 do
    for ty3=ty1,ty2,1 do
      set_rgb(tx3,ty3,tr1/255,tg1/255,tb1/255)
    end;end
  end
function COPY(tx1,ty1,tx2,ty2,tx3,ty3)
  for ty4=ty1,ty2,1 do
    for tx4=tx1,tx2,1 do
      r,g,b=get_rgb(tx4,ty4)
      set_rgb(tx3+tx4-tx1,ty3+ty4-ty1,r,g,b)
      end;end
  end

for y1=15,0,-1 do
  COPY(0,y1*8+0,127,y1*8+7,0,y1*128+0)
  progress ((15-y1)/30)
  end
flush()

for y1=0,15,1 do
  for x1=1,15,1 do
    COPY(x1*8,y1*128+0,x1*8+7,y1*128+7,0,y1*128+x1*8)
    progress(.5+(y1/30))
    end;end
flush()

BAR(8,0,width,height,0xFFFFFF)
flush()

