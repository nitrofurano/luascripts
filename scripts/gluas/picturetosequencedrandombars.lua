yclsz=16;xclsz0=4;xclsz1=32

function INT(a)
  return(a-a%1)
  end
function RND(tseed)
  tvl1=INT(tseed/256)
  tvl2=(((tseed%256)*256)+253)-(tseed*2)
  tseed=tseed%256+tvl1
  tvl2=tvl2-tseed
  while tvl2<0 do tvl2=tvl2+65537 end
  tvl2=tvl2%65536
  return tvl2
  end
function BOXHEXC(tx1,ty1,tx2,ty2,tcl)
  tr1=(INT(tcl/65536))%256
  tg1=(INT(tcl/256))%256
  tb1=tcl%256
  for tx3=tx1,tx2,1 do
    set_rgb(tx3,ty1,tr1/255,tg1/255,tb1/255)
    set_rgb(tx3,ty2,tr1/255,tg1/255,tb1/255)
    end
  for ty3=ty1,ty2,1 do
    set_rgb(tx1,ty3,tr1/255,tg1/255,tb1/255)
    set_rgb(tx2,ty3,tr1/255,tg1/255,tb1/255)
    end
  end
function BOXRGB(tx1,ty1,tx2,ty2,tr1,tg1,tb1)
  for tx3=tx1,tx2,1 do
    set_rgb(tx3,ty1,tr1/255,tg1/255,tb1/255)
    set_rgb(tx3,ty2,tr1/255,tg1/255,tb1/255)
    end
  for ty3=ty1,ty2,1 do
    set_rgb(tx1,ty3,tr1/255,tg1/255,tb1/255)
    set_rgb(tx2,ty3,tr1/255,tg1/255,tb1/255)
    end
  end
function BARHEXC(tx1,ty1,tx2,ty2,tcl)
  tr1=(INT(tcl/65536))%256
  tg1=(INT(tcl/256))%256
  tb1=tcl%256
  for tx3=tx1,tx2,1 do
    for ty3=ty1,ty2,1 do
      set_rgb(tx3,ty3,tr1/255,tg1/255,tb1/255)
    end;end
  end
function BARRGB(tx1,ty1,tx2,ty2,tr1,tg1,tb1)
  for tx3=tx1,tx2,1 do
    for ty3=ty1,ty2,1 do
      set_rgb(tx3,ty3,tr1/255,tg1/255,tb1/255)
    end;end
  end

yclsz2=INT(yclsz/2)
seed=0
ycnt=0
while ycnt<height do
  xcnt=0
  while xcnt<width do
    seed=RND(seed)
    xclsz=xclsz0+(seed%(xclsz1-xclsz0))
    xclsz2=INT(xclsz/2)
    x=xcnt+1
    y=ycnt+1
    r,g,b=get_rgb(x,y)
    BARRGB(xcnt,ycnt,xcnt+xclsz,ycnt+yclsz,r*255,g*255,b*255)
    xcnt=xcnt+xclsz
    end
  ycnt=ycnt+yclsz
	progress (ycnt/height)
  end


