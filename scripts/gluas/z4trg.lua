cellsiz=16
function INT(a)
  return(a-a%1)
  end
function HLINERGB(tx1,ty1,tx2,tr1,tg1,tb1)
  for tx3=tx1,tx2,1 do
    set_rgb(tx3,ty1,tr1,tg1,tb1)
    end
  end
function VLINERGB(tx1,ty1,ty2,tr1,tg1,tb1)
  for ty3=ty1,ty2,1 do
    set_rgb(tx1,ty3,tr1,tg1,tb1)
    end
  end
for y1=0,height-1,cellsiz do
  for x1=0,width-1,cellsiz do
    r0,g0,b0=get_rgb( x1+(cellsiz*.5), y1+(cellsiz*.25))
    r1,g1,b1=get_rgb( x1+(cellsiz*.75), y1+(cellsiz*.5))
    r2,g2,b2=get_rgb( x1+(cellsiz*.5), y1+(cellsiz*.75))
    r3,g3,b3=get_rgb( x1+(cellsiz*.25), y1+(cellsiz*.5))
    for con=0,(cellsiz*.5)-1,1 do
      HLINERGB(x1+con,          y1+con,          x1+cellsiz-con-1,r0,g0,b0)
      VLINERGB(x1+cellsiz-con-1,y1+con,          y1+cellsiz-con-1,r1,g1,b1)
      HLINERGB(x1+con,          y1+cellsiz-con-1,x1+cellsiz-con-1,r2,g2,b2)
      VLINERGB(x1+con,          y1+con,          y1+cellsiz-con-1,r3,g3,b3)
      end
    end
  progress(y/height)
  end
flush()

