--enlarge canvas size first
function INT(a)
  return(a-a%1)
  end
function BAR(tx1,ty1,tx2,ty2,tcl)
  tr1=(INT(tcl/65536))%256
  tg1=(INT(tcl/256))%256
  tb1=tcl%256
  for tx3=tx1,tx2,1 do
    for ty3=ty1,ty2,1 do
      set_rgb(tx3,ty3,tr1/255,tg1/255,tb1/255)
    end;end
  end
function COPY(tx1,ty1,tx2,ty2,tx3,ty3)
  for ty4=ty1,ty2,1 do
    for tx4=tx1,tx2,1 do
      r,g,b=get_rgb(tx4,ty4)
      set_rgb(tx3+tx4-tx1,ty3+ty4-ty1,r,g,b)
      end;end
  end
for y1=0,(height/16)-1,1 do
  COPY(0,y1*16+8,127,y1*16+15,128,y1*16)
  BAR(0,y1*16+8,127,y1*16+15,0xFFFFFF)
  end
flush()
for y1=1,(height/16)-1,1 do
  COPY(0,y1*16+0,255,y1*16+7,0,y1*8+0)
  BAR(0,y1*16+0,255,y1*16+7,0xFFFFFF)
  end
flush()












