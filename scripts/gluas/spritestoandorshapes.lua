--enlarge canvas size first
function INT(a)
  return(a-a%1)
  end
function BAR(tx1,ty1,tx2,ty2,tcl)
  tr1=(INT(tcl/65536))%256
  tg1=(INT(tcl/256))%256
  tb1=tcl%256
  for tx3=tx1,tx2,1 do
    for ty3=ty1,ty2,1 do
      set_rgb(tx3,ty3,tr1/255,tg1/255,tb1/255)
    end;end
  end
function COPY(tx1,ty1,tx2,ty2,tx3,ty3)
  for ty4=ty1,ty2,1 do
    for tx4=tx1,tx2,1 do
      r,g,b=get_rgb(tx4,ty4)
      set_rgb(tx3+tx4-tx1,ty3+ty4-ty1,r,g,b)
      end;end
  end

COPY(0,0,width,height/2,0,height/2)
flush()

for y=0,(height/2)-1,1 do
  for x=0,width-1,1 do
    r0,g0,b0=get_rgb(x,y)
    set_rgb(x,y,0,0,0)
    if r0>(240/255) and g0<(15/255) and b0>(240/255) then
      set_rgb(x,y,1,1,1)
      end
    end
  progress(y/height)
  end
flush()

for y=(height/2),height-1,1 do
  for x=0,width-1,1 do
    r0,g0,b0=get_rgb(x,y)
    -- set_rgb(x,y,0,0,0)
    if r0>(240/255) and g0<(15/255) and b0>(240/255) then
      set_rgb(x,y,0,0,0)
      end
    end
  progress(y/height)
  end
flush()






