xcell=8;ycell=8
function INT(a)
  return(a-a%1)
  end
for y=0,INT(height/ycell),1 do
  for x=0,INT(width/xcell),1 do
    rmin=255;rmax=0
    gmin=255;gmax=0
    bmin=255;bmax=0
    for y2=0,ycell-1,1 do
      for x2=0,xcell-1,1 do
        r,g,b=get_rgb(x*xcell+x2,y*ycell+y2)
        r=r*255;g=g*255;b=b*255
        if rmin>r then rmin=r end
        if gmin>g then gmin=g end
        if bmin>b then bmin=b end
        if rmax<r then rmax=r end
        if gmax<g then gmax=g end
        if bmax<b then bmax=b end
        end;end
    rmid=INT((rmax+rmin)/2)
    gmid=INT((gmax+gmin)/2)
    bmid=INT((bmax+bmin)/2)
    for y2=0,ycell,1 do
      for x2=0,xcell,1 do
        r,g,b=get_rgb(x*xcell+x2,y*ycell+y2)
        r=r*255;g=g*255;b=b*255
        r2=rmin;if r>rmid then r2=rmax end
        g2=gmin;if g>gmid then g2=gmax end
        b2=bmin;if b>bmid then b2=bmax end
        set_rgb(x*xcell+x2,y*ycell+y2,r2/255,g2/255,b2/255)
        end;end
    end
  progress((y*ycell)/height)
  end

