xcell=4;ycell=4


function INT(a)
  return(a-a%1)
  end
function SWAPPIXELS(tx0,ty0,tx1,ty1)
  r0,g0,b0=get_rgb(tx0,ty0)
  r1,g1,b1=get_rgb(tx1,ty1)
  set_rgb(tx1,ty1,r0,g0,b0)
  set_rgb(tx0,ty0,r1,g1,b1)
  end
function SWAPPIXELSSORTLUM(tx0,ty0,tx1,ty1)
  r0,g0,b0=get_rgb(tx0,ty0)
  r1,g1,b1=get_rgb(tx1,ty1)
  w0=(b0*11+r0*30+g0*59)/100
  w1=(b1*11+r1*30+g1*59)/100
  if w0>w1 then
    set_rgb(tx1,ty1,r0,g0,b0)
    set_rgb(tx0,ty0,r1,g1,b1)
    end
  end
function BAR(tx1,ty1,tx2,ty2,tcl)
  tr1=(INT(tcl/65536))%256
  tg1=(INT(tcl/256))%256
  tb1=tcl%256
  for tx3=tx1,tx2,1 do
    for ty3=ty1,ty2,1 do
      set_rgb(tx3,ty3,tr1/255,tg1/255,tb1/255)
    end;end
  end
function COPY(tx1,ty1,tx2,ty2,tx3,ty3)
  for ty4=ty1,ty2,1 do
    for tx4=tx1,tx2,1 do
      r,g,b=get_rgb(tx4,ty4)
      set_rgb(tx3+tx4-tx1,ty3+ty4-ty1,r,g,b)
      end;end
  end
function MOVESWAP(tx1,ty1,tx2,ty2,tx3,ty3)
  for ty4=ty1,ty2,1 do
    for tx4=tx1,tx2,1 do
      r0,g0,b0=get_rgb(tx4,ty4)
      r1,g1,b1=get_rgb(tx4-tx1+tx3,ty4-tx1+tx3)
      set_rgb(tx3+tx4-tx1,ty3+ty4-ty1,r0,g0,b0)
      set_rgb(tx4,ty4,r1,g1,b1)
      end;end
  end

xycell=xcell*ycell
for x1=0,width-1,xcell do
  for y1=0,height-1,ycell do
    for q2=0,xycell-2,1 do
      for q3=q2+1,xycell-1,1 do
        y2=INT(q2/xcell);y3=INT(q3/xcell)
        x2=q2%xcell;x3=q3%xcell
        x2s=x1+x2;y2s=y1+y2
        x3s=x1+x3;y3s=y1+y3
        SWAPPIXELSSORTLUM(x2s,y2s,x3s,y3s)
        end;end;end
  progress(x1/width)
  end
flush()


