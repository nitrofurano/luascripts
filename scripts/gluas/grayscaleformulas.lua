r=.333;g=.333;b=.333 --average
r=.3;g=.59;b=.11 --luminance
r=.2126;g=.7152;b=.0722 -- ITU-R BT.709
r=.299;g=.587;b=.114  -- ITU-R BT.601
for y=0,height-1,1 do
  for x=0,width-1,1 do
    r0,g0,b0=get_rgb(x,y)
    w0=((r0*r)+(g0*g)+(b0*b))/(r+g+b)
    set_rgb(x,y,w0,w0,w0)
    end
  progress(y/height)
  end
flush()

