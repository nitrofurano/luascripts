-- title: johannes itten colour contrast demo
-- author: nitrofurano
-- desc: johannes itten colour contrast demo
-- script: lua
cntr=0
cls()
function TIC()
 if (cntr % 8)==0 then

  i1=math.random(24)-1
  p1=math.random(256)-1
  poke (0x3FC0+i1,p1)

  x1=math.random(16)-1
  y1=math.random(8)-1
  c1=math.random(16)-1
  c2=math.random(16)-1

  rect(x1*16+2,y1*16+2+4,11,11,c1)
  rect(x1*16+4,y1*16+4+4,7,7,c2)

  end
 cntr=cntr+1
 end
