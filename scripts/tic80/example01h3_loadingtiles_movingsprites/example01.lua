-- title: spr test
-- author: nitrofurano
-- desc: spr test
-- script: lua

dx={} for i=0,255 do dx[i]=math.random(8)-4 end
dy={} for i=0,255 do dy[i]=math.random(8)-4 end
px={} for i=0,255 do px[i]=math.random(240) end
py={} for i=0,255 do py[i]=math.random(136) end

for q1=0x4000,0x5FFF,1 do poke(q1,(math.floor((peek(q1))/16))+(((peek(q1))%16)*16)) end
--for q1=0x6000,0x5FFF,1 do poke(q1,(math.floor((peek(q1))/16))+(((peek(q1))%16)*16)) end

cntr=0
cls()
function TIC()
 if (cntr % 2)==0 then
  mset(math.random(30)-1,math.random(17)-1,math.random(256)-1)
  map (0,0,30,17,0,0)
  end

 for i=0,255 do
  spr(i,px[i],py[i])
  px[i]=px[i]+dx[i]
  py[i]=py[i]+dy[i]

  if px[i]<0 then px[i]=px[i]+240 end
  if py[i]<0 then py[i]=py[i]+136 end
  if px[i]>239 then px[i]=px[i]-240 end
  if py[i]>135 then py[i]=py[i]-136 end

  end

 qq=math.random(256)-1
 dx[qq]=math.random(8)-4
 dy[qq]=math.random(8)-4

 cntr=cntr+1
 end


