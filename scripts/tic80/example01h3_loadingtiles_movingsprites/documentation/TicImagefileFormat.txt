https://itch.io/t/89912/tic-file-format-explain

(...)

for sure, the cartridge consists of chunks, every has 4-byte header 
(1 byte - type, 3 bytes - size) and data

chunk types:
0x01 - tiles
0x02 - sprites
0x03 - cover
0x04 - map
0x05 - code
0x09 - sfx
0x0A - waveforms
0x0B - music (?)
0x0C - music? 
0x0D - music patterns
0x0E - music?
(...)
-------------------------------------------------------

0x000000 0D 30 00 00 (=16*3)

?? ?? ??
vn I? oi   (v=invertvolume?,o=invertoctave*2+?,n=note+4?,i=instrument)

04 00 60 = C-400F--
14 00 60 = C-400E--
F4 00 60 = C-4000--
04 00 00 = C-100F--
14 00 20 = C-200E--
24 00 40 = C-300D--
34 00 60 = C-400C--
F4 00 E0 = C-8000--
00 00 00 = --------

vvvvnnnn i....... oooiiiii

-------------------------------------------------------
