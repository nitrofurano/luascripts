-- title: map mset test 2
-- author: nitrofurano
-- desc: map mset test 2
-- script: lua

cp1=math.random(540)-1
cp2=math.random(540)-1
cntr=0
cls()
function TIC()
 mset(cp1%30,math.floor(cp1/30)-1,math.random(256)-1)
 mset(cp2%30,math.floor(cp2/30)-1,32)
 cp1=(cp1+1)%540
 cp2=(cp2+1)%540
 if math.random(16)==8 then
  cp1=math.random(540)-1
  end
 if math.random(16)==8 then
  cp2=math.random(540)-1
  end
 map (0,0,30,17,0,0)
 end
