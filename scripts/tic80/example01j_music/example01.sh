$01spritesbg = '0'
$02spritesfg = '0'
$05luacode = '1'
$0awaveform = '1'
$0cpalette = '1'
$0dtunesnotes = '1'
$0etunesframes = '1'

if [$01spritesbg != '0']
then
#- 01 - sprites background (tiles)
xxd -r -p ./library/01_sprites_bg.hex > 01_sprites_bg.bin
filesize=`du -b "01_sprites_bg.bin" | cut -f1`
printf "0: %.6x" $filesize | xxd -r -g0 > 01_3bytes_.bin
echo 0:01 | xxd -r > 01_byte0_.bin
dd ibs=1 count=1 skip=0 if=01_3bytes_.bin of=01_byte3_.bin
dd ibs=1 count=1 skip=1 if=01_3bytes_.bin of=01_byte2_.bin
dd ibs=1 count=1 skip=2 if=01_3bytes_.bin of=01_byte1_.bin
cat 01_byte0_.bin 01_byte1_.bin 01_byte2_.bin 01_byte3_.bin 01_sprites_bg.bin > 01_all.bin
rm 01_*_.bin
rm 01_sprites_bg.bin
sleep 1
fi

if [$02spritesfg != '0']
then
#- 02 - sprites foreground
xxd -r -p ./library/02_sprites_fg.hex > 02_sprites_fg.bin
filesize=`du -b "02_sprites_fg.bin" | cut -f1`
printf "0: %.6x" $filesize | xxd -r -g0 > 02_3bytes_.bin
echo 0:02 | xxd -r > 02_byte0_.bin
dd ibs=1 count=1 skip=0 if=02_3bytes_.bin of=02_byte3_.bin
dd ibs=1 count=1 skip=1 if=02_3bytes_.bin of=02_byte2_.bin
dd ibs=1 count=1 skip=2 if=02_3bytes_.bin of=02_byte1_.bin
cat 02_byte0_.bin 02_byte1_.bin 02_byte2_.bin 02_byte3_.bin 02_sprites_fg.bin > 02_all.bin
rm 02_*_.bin
rm 02_sprites_fg.bin
sleep 1
fi

#- 03 - cover (?)

#- 04 - map (?)

#- 05 - Lua code
filesize=`du -b "example01.lua" | cut -f1`
printf "0: %.6x" $filesize | xxd -r -g0 > 05_3bytes_.bin
echo 0:05 | xxd -r > 05_byte0_.bin
dd ibs=1 count=1 skip=0 if=05_3bytes_.bin of=05_byte3_.bin
dd ibs=1 count=1 skip=1 if=05_3bytes_.bin of=05_byte2_.bin
dd ibs=1 count=1 skip=2 if=05_3bytes_.bin of=05_byte1_.bin
cat 05_byte0_.bin 05_byte1_.bin 05_byte2_.bin 05_byte3_.bin example01.lua > 05_all.bin
rm 05_*_.bin
sleep 1

#- 06 - ?

#- 07 - ?

#- 08 - ?

#- 09 - sfx (?)

#- 0A - waveform (?)
xxd -r -p ./library/waveform.hex > waveform.bin
filesize=`du -b "waveform.bin" | cut -f1`
printf "0: %.6x" $filesize | xxd -r -g0 > 0A_3bytes_.bin
echo 0:0A | xxd -r > 0A_byte0_.bin
dd ibs=1 count=1 skip=0 if=0A_3bytes_.bin of=0A_byte3_.bin
dd ibs=1 count=1 skip=1 if=0A_3bytes_.bin of=0A_byte2_.bin
dd ibs=1 count=1 skip=2 if=0A_3bytes_.bin of=0A_byte1_.bin
cat 0A_byte0_.bin 0A_byte1_.bin 0A_byte2_.bin 0A_byte3_.bin waveform.bin > 0A_all.bin
rm 0A_*_.bin
rm waveform.bin
sleep 1

#- 0B - ?

#- 0C - palette
xxd -r -p ./library/palette.hex > palette.bin
filesize=`du -b "palette.bin" | cut -f1`
printf "0: %.6x" $filesize | xxd -r -g0 > 0C_3bytes_.bin
echo 0:0C | xxd -r > 0C_byte0_.bin
dd ibs=1 count=1 skip=0 if=0C_3bytes_.bin of=0C_byte3_.bin
dd ibs=1 count=1 skip=1 if=0C_3bytes_.bin of=0C_byte2_.bin
dd ibs=1 count=1 skip=2 if=0C_3bytes_.bin of=0C_byte1_.bin
cat 0C_byte0_.bin 0C_byte1_.bin 0C_byte2_.bin 0C_byte3_.bin palette.bin > 0C_all.bin
rm 0C_*_.bin
rm palette.bin
sleep 1

#- 0D - tunes: notes
xxd -r -p ./library/tunes_notes.hex > tunes_notes.bin
filesize=`du -b "tunes_notes.bin" | cut -f1`
printf "0: %.6x" $filesize | xxd -r -g0 > 0D_3bytes_.bin
echo 0:0D | xxd -r > 0D_byte0_.bin
dd ibs=1 count=1 skip=0 if=0D_3bytes_.bin of=0D_byte3_.bin
dd ibs=1 count=1 skip=1 if=0D_3bytes_.bin of=0D_byte2_.bin
dd ibs=1 count=1 skip=2 if=0D_3bytes_.bin of=0D_byte1_.bin
cat 0D_byte0_.bin 0D_byte1_.bin 0D_byte2_.bin 0D_byte3_.bin tunes_notes.bin > 0D_all.bin
rm 0D_*_.bin
rm tunes_notes.bin
sleep 1

#- 0E - tunes: frames
xxd -r -p ./library/tunes_frames.hex > tunes_frames.bin
filesize=`du -b "tunes_frames.bin" | cut -f1`
printf "0: %.6x" $filesize | xxd -r -g0 > 0E_3bytes_.bin
echo 0:0E | xxd -r > 0E_byte0_.bin
dd ibs=1 count=1 skip=0 if=0E_3bytes_.bin of=0E_byte3_.bin
dd ibs=1 count=1 skip=1 if=0E_3bytes_.bin of=0E_byte2_.bin
dd ibs=1 count=1 skip=2 if=0E_3bytes_.bin of=0E_byte1_.bin
cat 0E_byte0_.bin 0E_byte1_.bin 0E_byte2_.bin 0E_byte3_.bin tunes_frames.bin > 0E_all.bin
rm 0E_*_.bin
rm tunes_frames.bin
sleep 1

#- 0F - ?

cat 05_all.bin 0D_all.bin 0E_all.bin 01_all.bin 02_all.bin 0C_all.bin 0A_all.bin > example01.tic
rm 05_all.bin 0D_all.bin 0E_all.bin 01_all.bin 02_all.bin 0C_all.bin 0A_all.bin
sleep 1

tic80 example01.tic
# tic80 -fullscreen example01.tic still doesn't work... :(

